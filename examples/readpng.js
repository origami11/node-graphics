var 
    Bitmap = require('../lib/bitmap.js').Bitmap,
    g = require('../lib/graphics.js');

Bitmap.loadFromPNG('images/trees.png', function (src) {
    var dst = new Bitmap(src.width, src.height);

    dst.render(
        g.supersampling(
            g.mixin(
                function (i, j) {
                    var c = src.getPixel(Math.floor(i), Math.floor(j));        
                    return c;
                },
                g.color(g.ellipse(90, 160, 40, 60), [255, 0, 0, 128]),
                g.color(g.ellipse(150, 160, 60, 40), [255, 255, 0, 128])
            ),
        3)
    );

    dst.saveAsPNG('test.png');
});
