var ppm = require('../lib/bitmap.js');

var bmp = new ppm.Bitmap(640, 400);
bmp.clear();

function distance(x, y) {
    return Math.sqrt(x * x + y * y);
}

bmp.render(function (i, j) {
    var r = distance(i - 100, j - 100); 
    return [0, 0, 100* Math.sin(r/10) + 128, 255];
});

bmp.saveAsPNG('image.png');
bmp.saveAsJPEG('image.jpg', 90);