/* */

/**
 * Супер семплинг
 * supersampling: figure * quality -> (coord -> color)
 */
function supersampling(c, N) {
    return function (x, y) {
        var k = [0, 0, 0, 0], color = false;
        var S = 0;        
        var N1 = N + 1;
        for(var i = 1; i < N1; i++) {
            for(var j = 1; j < N1; j++) {
                color = c(x + i/N1, y + j/N1)
                if (color) {
                    k[0] += color[0] * color[3] / 255;
                    k[1] += color[1] * color[3] / 255;
                    k[2] += color[2] * color[3] / 255;
                    k[3] += color[3];
                    S += (color[3] / 255);
                } 
            }
        }
        var N2 = N * N;
        return [k[0] / S, k[1] / S, k[2] / S, k[3]/N2];
    }
}

/**
 * Градиент
 */
function gradient(source, direction, from_color, to_color) {
    return function (i, j) {
    }
}

function colorize(fn, def, bg) {
    return function (i, j) {
        var cn = fn(i, j);
        if (cn) {
            if (cn === true) {
                return def(i, j);
            }
            return cn;
        }
        return bg;
    }
}

/**
 * Сплошной цвет
 * color: figure * color -> (coord -> color)
 */
function color(fn, c, def) {
    def = (arguments.length == 3) ? def : false;
    return function (i, j) {
        if (fn(i, j)) {
            return c;
        }
        return def;
    }
}

/**
 * Принадлежность точки полигону
 */
function pnpoly(vertx, verty, testx, testy) {
    var nvert = vertx.length;
    var i, j, c = false;
    for (var i = 0, j = nvert-1; i < nvert; j = i++) {
        if ( ((verty[i]>testy) != (verty[j]>testy)) &&
    	 (testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) ) {
           c = !c;
        }
    }
    return c;
}

/**
 * Окружность
 * circle: number * number * number -> (coord -> color)
 * @param x,y Координаты цента окружности
 * @param r Радиус сегмента окружности
 */
function circle(x, y, r) {  
    return function (i, j) {
        var dx = x - i;
        var dy = y - j;
        return Math.sqrt(dx * dx + dy * dy) < r;
    }
}


/**
 * Эллипс
 */
function ellipse(x, y, a, b) {  
    return function (i, j) {
        var dx = (x - i) / a;
        var dy = (y - j) / b;
        return Math.sqrt(dx * dx + dy * dy) < 1;
    }
}


/**
 * Часть окружности
 * @param x,y Координаты цента окружности
 * @param r Радиус сегмента окружности
 * @param sa, ea Углы ограницивающие окружность
 */
function segment(x, y, r, sa, ea) {
   var sy = Math.sin(sa), sx = Math.cos(sa);
   var ey = Math.sin(ea), ex = Math.cos(ea);
   return function (i, j) {
        var dx = x - i;
        var dy = y - j;
        var pa = (dx * sy - dy * sx > 0), pb = (dx * ey - dy * ex < 0);

        return ((Math.abs(ea - sa) > Math.PI) ? pa || pb : pa && pb) && Math.sqrt(dx * dx + dy * dy) < r;
    }
}

/**
 * Многоугольник
 * @param x,y Координаты цента правильного многоугольника
 * @param rin Радиус описанной окружности
 * @param N Количество сторон
 * @param a0 угол поворота фигуры
 */
function wheel(N, x, y, rin, a0) {
    var polyx = [], polyy = [];
    var da = Math.PI * 2 / N;

    for(var i = 0; i < N; i++) {
        polyx.push(x + rin * Math.cos(a0 + i * da));
        polyy.push(y + rin * Math.sin(a0 + i * da));
    }

    return [polyx, polyy];
}

/**
 * Звезда
 * @param N Количество сторон
 * @param x,y Координаты цента правильного многоугольника
 * @param rout Радиус описанной окружности 
 * @param rin Радиус вписанной окружности 
 * @param a0 угол поворота фигуры
 */
function star(N, x, y, rin, rout, a0) {
    var polyx = [], polyy = [];
    var da = Math.PI * 2 / N;

    for(var i = 0; i < N; i++) {
        polyx.push(x + rin * Math.cos(a0 + i * da));
        polyy.push(y + rin * Math.sin(a0 + i * da));

        polyx.push(x + rout * Math.cos(a0 + i * da + da / 2));
        polyy.push(y + rout * Math.sin(a0 + i * da + da / 2));
    }

    return [polyx, polyy];
}

/**
 * Преобразования многоугольника в фигуру
 */
function fillpoly(p) {
    return function (i, j) {
        return pnpoly(p[0], p[1], i, j);
    }
}

function between(x, a, b) {
    if (a > b) {
        return x >= b && x <= a; 
    }
    return x >= a && x <= b; 
}

/**
 * Полуплоскость
 * @param x,y Точка прямой которая разделяет плоскость попалам
 * @param a Угол наклона прямой
 */
function half_plane(x, y, a) {
   var sy = Math.sin(a), sx = Math.cos(a);
    return function (i, j) {
        var dx = x - i;
        var dy = y - j;
        return dx * sy - dy * sx > 0;
    }
}

/**
 * Отрезок
 * @param x1,y1 Координаты начала отрезка
 * @param x2,y2 Координаты конца отрезка
 * @param d Толщина отрезка
 */
function line(x1, y1, x2, y2, d) {
    var dx = x2 - x1, dy = y2 - y1;
    var r = Math.sqrt(dx * dx  + dy  * dy);
    return function (i, j) {
        // Исправить условия для канцов линии
        return (between(i, x2, x1) || Math.abs(x2 - x1) < d/2) && 
            (between(j, y2, y1) || Math.abs(y2 - y1) < d/2) && Math.abs(dx * (y1 - j) - dy * (x1 - i)) / r <= d; 
    }
}

/**
 * Разность фигур
 */
function diff(a, b) {
    return function (i, j) {
        return a(i, j) && !b(i, j);
    }
}

function diffn() {
    var args = arguments;
    return function (i, j) {
        var c = args[0](i, j);
        if (c) {
            for (var k = 1; k < args.length; k++) {
                if (args[k](i, j)) return false;
            }
        }
        return c;
    }
}

/**
 * Общая часть двух вигур
 */
function and(a, b) {
    return function (i, j) {
        return a(i, j) && b(i, j);
    }
}

/**
 * Обьединение фигур (без учета прозрачности)
 */
function union() {
    var args = arguments;
    return function (i, j) {
        var d = 0;
        for(var k = 0; k < args.length; k++) {
            d = args[k](i, j);
            if (d) return d;
        }
        return d;
    }
}


/**
 * Обьединение фигур с учетом прозрачности
 *  c0 = ca * Aa + cb * Ab (1 - Aa)
 *  A0 = Aa + Ab * (1 - Aa)
 */
function mixin() {
    var args = arguments;
    return function (i, j) {
        var d = [0, 0, 0, 0], Ac = 0, Ad = 0;
        for(var k = 0; k < args.length; k++) {
            var c = args[k](i, j);
            if (c) {
                Ac = c[3] / 255;
                Ad = d[3] / 255;
                d[0] = c[0] * Ac + d[0] * Ad * (1 - Ac);
                d[1] = c[1] * Ac + d[1] * Ad * (1 - Ac);
                d[2] = c[2] * Ac + d[2] * Ad * (1 - Ac);

                d[3] = (Ac + Ad * (1 - Ac)) * 255;
            }
        }
        return d;
    }
}

/**
 * Прямоугольник
 * @param x,y Вершина прямоугольника
 * @param w Ширина прямоугольника
 * @param h Высота прямоугольника
 */
function rectangle(x, y, w, h) {
    return function (i, j) {
        return between(i, x, x + w) && between(j, y, y + h);
    }
}

/**
 * Перемещение фигуры
 * @param x,y Смещение фигуры
 */
function move(x, y, c) {
    return function (i, j) {
        return c(i - x, j - y);
    }
}

/**
 * Изменение масштаба
 * @param x,y Центр масштаба
 * @param n Коэффицент увеличения
 */
function scale(x, y, n, c) {
    return function (i, j) {
        return c((i-x)/n + x, (j - y)/n + y);
    }
}

/**
 * Поворот фигуры
 * @param x,y Точка относительно которой поворачивается фигура
 * @param a Угол поворота фигуры
 */
function rotate(x, y, a, c) {
    var ca = Math.cos(a), sa = Math.sin(a);
    return function (i, j) {
        var dx = i - x;
        var dy = j - y;
        return c(dx * ca - dy * sa + x, dx * sa + dy * ca + y);
    }
}

/**
 * Отражение фигуры по оси X
 */
function mirrorX(X, c) {
    return function (i, j) {
        return c(2 *X - i, j);
    }
}

/**
 * Отражение фигуры по оси Y
 */
function mirrorY(Y, c) {
    return function (i, j) {
        return c(i, 2*Y - j);
    }
}

/**
 * Кольцо
 * @param x,y Координаты цента кольца
 * @param rin Внутренний радиус
 * @param rout Внешний радиус
 */
function ring(x, y, rin, rout) {
    return diff(circle(x, y, rout), circle(x, y, rin));
}

/**
 * Прямоугольник с закругленными концами
 */
function round_rect(x, y, w, h, r0) {
    var r = (Array.isArray(r0)) ? r0 : [r0, r0, r0, r0];
    return union(
        circle(x + r[0], y + r[0], r[0]),
        circle(x + w- r[1], y + r[1], r[1]),
        circle(x + w- r[2], y + h -r[2], r[2]),
        circle(x + r[3], y + h-r[3], r[3]),
        diffn(
            rectangle(x, y, w, h),
            rectangle(x, y, r[0], r[0]),
            rectangle(x+w-r[1], y, r[1], r[1]),
            rectangle(x+w-r[2], y+h-r[2], r[2], r[2]),
            rectangle(x, y+h-r[3], r[3], r[3])
        ));
}

/**
 * Преобразование градусов в радианы
 */
function deg2rad(deg) {
    return Math.PI / 180 * deg;
}

/**
 * Арка
 */
function arc(x, y, rin, rout, sa, ea) {
    return diff(segment(x, y, rout, sa, ea), circle(x, y, rin));
}

exports.supersampling = supersampling;
exports.gradient = gradient;
exports.color = color;
exports.pnpoly = pnpoly;
exports.circle = circle;
exports.ellipse = ellipse;
exports.segment = segment;
exports.wheel = wheel;
exports.star = star;
exports.fillpoly = fillpoly;
exports.between = between;
exports.line = line;
exports.diff = diff;
exports.diffn = diffn;
exports.and = and;
exports.union = union;
exports.rectangle = rectangle;
exports.move = move;
exports.rotate = rotate;
exports.mirrorX = mirrorX;
exports.mirrorY = mirrorY;
exports.ring = ring;
exports.round_rect = round_rect;
exports.deg2rad = deg2rad;
exports.arc = arc;
exports.half_plane = half_plane;
exports.colorize = colorize;
exports.scale = scale;
exports.mixin = mixin;
