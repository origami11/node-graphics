
var ppm = require('../lib/bitmap.js')
    , g = require('../lib/graphics.js')
    , path = require('path')
    , fs = require('fs');


var fillpoly = g.fillpoly;
var cirlce = g.circle;
var union = g.union;
var diff = g.diff;
var and = g.and;
var rotate = g.rotate;
var rectangle = g.rectangle;
var arc = g.arc;
var line = g.line;
var deg2rad = g.deg2rad;
var move = g.move;
var round_rect = g.round_rect;
var ring = g.ring;
var circle = g.circle;
var ss = g.supersampling;

/**
 * Трапеция
 * @param w Высота трапеции
 * @param a, b Стороны трапеции
 */
function trapeze(w, b, a) { 
    return [[0, w, w, 0], [b/2, a/2, -a/2, -b/2]];
}

function triangle(n) {
    return [[0, n, 0], [0, 0, n]];
}

function hat(x, y, hx, hy) {
    return fillpoly([[x-hx/2, x, x+hx/2], [y, y - hy, y]]);
}

//*  ///
function wheel_icon (x, y) {
    var d = Math.PI/8;
    var c = union(
        diff(
            union(
                fillpoly(g.wheel(8, x, y, 7.5, Math.PI/8 + d)),
                and(
                    fillpoly(g.star(8, x, y, 4.5, 40, 0 + d)),
                    fillpoly(g.wheel(8, x, y, 10.5, 0 + d))
                )),
            circle(x, y, 4)
        ),
        circle(x, y, 1.8)
    );
    return c;
}

function arrow_icon(x, y) {
    return union(
        move(x, y, fillpoly(triangle(5))),
        line(x, y, x + 4.9, y + 4.9, 0.7)
    );
}

function home_icon(x, y) {
    return union(
        diff(rectangle(x, y, 14, 10), rectangle(x+5, y+2, 14-10, 10-5)), 
        rectangle(x+1, y-7, 3, 5),
        hat(x+7, y, 20, 8)
    );
}

function zoom_icon(x, y) {
    return union(
        diff(circle(x, y, 4.5), circle(x, y, 2.5)),
        line(x- 3.4, y + 3.4, x - 7, y + 7, 1)
    );
}

var a1 = arrow_icon(3, 3);
var full_top = union(a1, g.mirrorX(12, a1));
var full_icon = union(full_top, g.mirrorY(12, full_top));

var a2 = arrow_icon(16, 16)
var normal_top = union(a2, g.mirrorX(12, a2));
var normal_icon = union(normal_top, g.mirrorY(12, normal_top));

function arrow_right(x, y, hx, hy) {
    return fillpoly([[x, x + hx, x], [y - hy/2, y, y+hy/2]]);
}

function plus_icon(d, h)  {
    return union(
        line(0, -d, 0, d, h),
        line(-d, 0, d, 0, h)
    );
}

function cross_icon(r, h) {
    return union(
        line(-r, -r, +r, +r, h),
        line(-r, +r, +r, -r, h)
    );
}

var right = arrow_right(0, 0, 6, 10);
var forward = union(move(6, 12, right), move(12, 12, right));
var backward = g.mirrorX(12, forward);

var big_right = arrow_right(0, 0, 8, 12);
var right_icon = union(move(12, 12, big_right), rectangle(6, 12-2, 7, 4))

function zoom_in(x, y) {
    return union(
        ring(x, y, 5, 7),
        move(x, y, plus_icon(3, 0.8)),
        line(x - 4, y + 4, x - 10, y + 10, 1.5)
    );
}

function camera_icon() {
    return move(0, -3, union(round_rect(12-3, 8, 6, 4, 2),
            diff(round_rect(3, 10, 18, 12, 3),
                 ring(12, 16, 2.7, 4))));
}

/**
 * Иконка динамиков
 */
function sound_icon() {
    return union(
        move(8, 12, fillpoly(trapeze(5, 6, 16))),
        rectangle(5, 12-3, 2, 6),
        arc(12, 12, 3.8, 4.6, deg2rad(-60), deg2rad(60)),
        arc(12, 12, 6, 7, deg2rad(-70), deg2rad(70))
    );
}

function tv_icon() {
    var h = 8;
    return move(0, 1, union(
        g.color(rectangle(12-h+1, 5, 2*(h-1), 10), [160, 160, 160, 255]),
        line(12, 12, 12, 18, 2),
        line(12-4, 18, 12+4, 18, 1),
        rectangle(12-h, 4, 2*h, 12)
    ));
}

function reload_icon() {
    return union(
        diff(arc(12, 12, 3.9, 7.1, -deg2rad(90+80), deg2rad(90+60)),
            line(0,12,12, 12, 1.8)
        ),
        move(4, 12, fillpoly(triangle(7)))
    );
}

function man_icon() {
    return move(0, 1, union(
        circle(12, 8, 4.5),
        diff(round_rect(4, 12, 16, 10, 4.5), rectangle(4, 19, 16, 3))
    ));
}

function muzic_icon() {
    return union(
        arc(12, 9, 5.5, 6.5, -deg2rad(180), 0),        
        rectangle(5, 7, 2, 3),
        rectangle(17, 7, 2, 3),
        man_icon()
    );
}

function pointer_icon() {
    var h = 11, d = 6;
    return move(0.3-1, 0, rotate(12, 12, -deg2rad(61), union(
        move(3, 12, fillpoly([[0, h, h-2.5, h], [0, d, 0, -d]])),
        line(5, 12, 19, 12, 1.2)
    )));
}

function pointer_to_dot_icon() {
    var r = ring(7, 4, 2, 3);
    return move(2, 2, union( pointer_icon(), g.color(r, [255, 0, 0, 255])));
}

function pen_line_icon() {
}

function pen_icon() {
    var h = 6;
    return rotate(12, 12, deg2rad(60),
        move(3, 12, union(
            fillpoly(trapeze(5, 1, 4)), 
            g.color(fillpoly(trapeze(8, 1, h)), [200, 200, 200, 255]),
            g.color(rectangle(17, -3, 2, h), [140, 140, 140, 255]),
            rectangle(8, -3, 11, h)
        )));
}

function microphone_icon() {
    var p = 8;
    return union(
        round_rect(8, 4, p, p+4, 3),        
        line(12, 17, 12, 21, 1),
        line(12-4, 20, 12+4, 21, 1),
        g.diffn(
            round_rect(6, 2, p+4, p+8, 3),
            round_rect(7, 3, p+2, p+6, 3),
            rectangle(0, 0, 24, 10)
        )
    );
}

function rss_icon() {
    var h = 8;
    return union(
        g.diffn(round_rect(12-h, 12-h, 2*h, 2*h, 4),
            circle(8, 16, 1.5),
            arc(7, 17, 4, 5.5, deg2rad(-90), deg2rad(0)),
            arc(7, 17, 7, 8.5, deg2rad(-90), deg2rad(0))
        )
    );
}

function eraser_icon() {
    return union(
            move(7, 4, rotate(12, 12, deg2rad(40), 
                g.diffn(
                    round_rect(0, 0, 18, 8, 4),
                    g.half_plane(0, 4, deg2rad(45)),
                    round_rect(10, 1, 7, 6, [0, 3, 3, 0])
            ))),
            g.color(line(8, 17, 21, 18, 1), [255, 0, 0, 255])
        );
}

function pause_icon() {
    return move(8, 7, union(
        rectangle(0, 0, 3, 10),
        rectangle(5, 0, 3, 10)
    ));
}

function video_icon() {
    return move(1, 5, union(
        diff(circle(5, 3, 4), ring(5, 3, 1, 2)),
        diff(circle(14, 3, 4), ring(14, 3, 1, 2)),
        round_rect(3, 7, 13, 8, 1),
        move(13, 11, fillpoly(trapeze(8, 0, 8)))
    ));
}

function heart_icon() {
    return move(0, -1, g.colorize(union(
        move(12, 14, rotate(0, 0, deg2rad(45), rectangle(-5, -5, 10, 10))),
        circle(8, 10, 5), circle(16, 10, 5)
    ), yfill([250, 250, 250, 255], [255, 0, 0, 255], 24), false));
}

function closed() {
    var r = 8, d = r/Math.sqrt(2);
    return g.color(union(
        ring(12, 12, r-1, r),
        line(12-d, 12-d, 12+d, 12+d, 0.5)
    ), [255, 0, 0, 255]);
}

function fill(c) {
    return function (i, j) {
        return c;
    }
}

function yfill(c1, c2, step) {
    return function (i, j) {
        var k = j / step, k1 = 1 - k;
        return [
            c1[0] * k1 + c2[0] * k, 
            c1[1] * k1 + c2[1] * k, 
            c1[2] * k1 + c2[2] * k, 
            c1[3] * k1 + c2[3] * k 
        ];
    }
}

function saveIcons(icon, scale, n) {
    if (!fs.existsSync('output')) {
        fs.mkdirSync('output');
    }    
    /* output icons */
    var bmp = new ppm.Bitmap(24 * scale, 24 * scale);
    n = n || icon.length;
    for(var i = 0; i < n; i++) {
        bmp.clear();
        bmp.render(ss(g.colorize(g.scale(0, 0, scale, icon[i][1]), yfill([150, 150, 150, 255], [68, 68, 68, 255], 24*scale), false), 7));
        bmp.saveAsPNG('output/' + icon[i][0]);
    }
}

function saveToFile(icon, cols) {
    var rows = Math.ceil(icon.length/cols);
    var bmp = new ppm.Bitmap(24 * cols, 24 * rows);    
    var temp = new ppm.Bitmap(24, 24, true);           
    var all = [];
    var nr = 0, nc = 0;
    for(var i = 0; i < icon.length; i++) {
        temp.clear();
        temp.render(ss(g.colorize(icon[i][1], yfill([150, 150, 150, 255], [68, 68, 68, 255], 24), [255, 255, 255, 255]), 4));
        temp.copy(bmp, 0, 0, 24 * nc , 24* nr, 24, 24);
        if (nc == cols - 1) { nc = 0; nr ++; } else { nc ++; }
    }
    bmp.saveAsPNG('output.png');
}

function no_icon(c) {
    return union(
        g.color(line(22, 2, 2, 22, 0.7), [255, 0, 0, 255]), 
        diff(c, line(22, 2, 2, 22, 2))
    );
}

function hand_icon() {
    var fw = 2.7, fd = 3;
    return move(4, 0,  
        union(
            round_rect(4, 2, fw, 12, 1.3),
            round_rect(4+1*fd, 6, fw, 10, 1),
            round_rect(4+2*fd, 7, fw, 10, 1),
            round_rect(4+3*fd, 8, fw, 10, 1),

            move(0, 11, g.rotate(0, 0, g.deg2rad(30), round_rect(0, 0, 3, 11, 1))),
            //
            round_rect(4, 10, fd*4-0.2, 11, 5)
        )
    );
}

var icon = [
    ['hand.png',
//        g.union(
            hand_icon()
//            g.color(g.scale(12, 12, 1.2, hand_icon()), [255, 255, 255, 255])
//        )
    ],
    ['heart.png', heart_icon()],
    ['no_sound.png', no_icon(sound_icon())],
    ['no_microphone.png', no_icon(microphone_icon())],
    ['no_video.png', no_icon(video_icon())],
    ['stop.png', rectangle(7, 7, 10, 10)],
    ['closed.png', closed()],
    ['video.png', video_icon()],
    ['pause.png', pause_icon()],
    ['play.png', move(8, 12, fillpoly(trapeze(10, 10, 0)))],
    ['smallcross.png', move(12, 12, g.color(cross_icon(3, 0.6), [255, 0, 0, 255]))],
    ['eraser.png', eraser_icon()],
    ['drawpoly.png', union(move(1, 0, pen_icon()), g.color(arc(12, 12, 8, 9, deg2rad(40), deg2rad(240)), [255, 0, 0, 255]))],
    ['drawline.png', union(move(1, 0, pen_icon()), g.color(line(2, 10, 9, 23, 0.5), [255, 0, 0, 255]))],
    ['pen.png', pen_icon()],
    ['favorite.png', move(0, 0.1, fillpoly(g.star(5, 12, 13, 4, 10, Math.PI/10)))],
    ['pointdot.png', pointer_to_dot_icon()],
    ['pointer.png', pointer_icon()],
    ['microphone.png', microphone_icon()],
    ['rss.png', union(g.color(rss_icon(), [255, 165, 0, 255]))],
    ['muzic.png', muzic_icon()],
    ['man.png', man_icon()],
    ['reload.png', reload_icon()],
    ['tv.png', tv_icon()],
    ['sound.png', sound_icon()],
    ['camera.png', camera_icon()],   
    ['zoom_in.png', zoom_in(14, 9)],   
    ['wheel.png', wheel_icon(12, 12)],
    ['zoom.png', zoom_icon(13, 10)],
    ['home.png', home_icon(5, 11)],
    ['full.png', full_icon],
    ['normal.png', normal_icon],
    ['plus.png', move(12, 12, plus_icon(4, 1))],
    ['cross.png', move(12, 12, g.color(cross_icon(4, 1), [255, 0, 0, 255]))],
    ['forward.png', forward],
    ['backward.png', backward],
    ['right.png', right_icon],
    ['left.png', g.mirrorX(12, right_icon)],
];


//saveToFile(icon, 4);
saveIcons(icon, 1, 0);
