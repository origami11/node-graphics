var ppm = require('../lib/bitmap.js');

//var bmp = new ppm.Bitmap(640*2, 400*2)
var bmp = new ppm.Bitmap(1920, 1080);
bmp.clear();

function scale(x, min, max, size) {
    return x * (max - min)/size + min;
}

var max_iteration = 255;

bmp.render(function (i, j) {
    var x0 = scale(i, -2.5, 1, bmp.width); 
    var y0 = scale(j, -1, 1, bmp.height); 

    var x = 0;
    var y = 0;

    var iteration = 0;

    while ( x*x + y*y < 4  &&  iteration < max_iteration ) {
        var xtemp = x*x - y*y + x0;
        y = 2*x*y + y0;
        x = xtemp;

        iteration ++;
    }

    color = iteration;
    return [255 - color, 255 - color, 255 - color, 255];
});

bmp.saveAsPNG('image.png');