var fs = require('fs');

function Bitmap(width, height, data) {
    this.width = width;
    this.height = height;
    this.bpp = 4;
    this.data = data || Buffer.alloc(width * height * this.bpp);
}

Bitmap.prototype.putColor = function (x, y, c) {
    this.putPixel(x, y, c[0], c[1], c[2], c[3]);
}

Bitmap.prototype.putPixel = function (x, y, r, g, b, a) {
    x = Math.ceil(x);
    y = Math.ceil(y);
    var offset = (x + y * this.width) * this.bpp;

    this.data[offset + 0] = Math.floor(r);
    this.data[offset + 1] = Math.floor(g);
    this.data[offset + 2] = Math.floor(b);
    this.data[offset + 3] = (arguments.length == 6) ? Math.floor(a) : 255;
}

Bitmap.prototype.copy = function (dst, sx, sy, ex, ey, w, h) {
    for(var i = 0; i < w; i++) {
        for(var j = 0; j < h; j++) {
            dst.putColor(ex + i, ey + j, this.getPixel(sx + i, sy + j));
        }
    }
}

Bitmap.prototype.getPixel = function (x, y) {
    var offset = (x + y * this.width) * this.bpp;

    return [
        this.data[offset + 0],
        this.data[offset + 1],
        this.data[offset + 2],
        this.data[offset + 3]
    ];
}

Bitmap.prototype.clear = function () {
    var i;
    for(i = 0; i < this.data.length; i++) {
        this.data[i] = 0;       
    }
}

Bitmap.prototype.fill = function (r, g, b, a) {
    var i;
    for(i = 0; i < this.data.length; i += this.bpp) {
        this.data[i + 0] = r;
        this.data[i + 1] = g;
        this.data[i + 2] = b;
        this.data[offset + 3] = (arguments.length == 4) ? Math.floor(a) : 255;
    }
}

Bitmap.prototype.render = function (fn) {
    var i, j = 0;
    for(i = 0; i < this.width; i++) {
        for(j = 0; j < this.height; j++) {
            this.putColor(i, j, fn.call(this, i, j));
        }
    }
}

Bitmap.prototype.saveAsPPM = function (name) {
    var fd =  fs.openSync(name, 'w');
    if (this.bpp == 4) throw '"ppm" does not support alpha channel';

    fs.writeSync(fd, 'P6\n' + this.width + '\n' + this.height + '\n' + 255 + '\n');
    fs.writeSync(fd, this.data, 0, this.data.length);
    fs.closeSync(fd);
}

Bitmap.prototype.saveAsPNG = function (name) {
    var png = require('./pngencoder.js');
    png.encode(this, function (data) {
        var fd =  fs.openSync(name, 'w');
        for(var i = 0; i < data.length; i++) {
            fs.writeSync(fd, data[i], 0, data[i].length);
        }
        fs.closeSync(fd);
    });
}

Bitmap.prototype.saveAsJPEG = function (name, quality) {
    var jpg = require('./jpegencoder.js');
    var data = jpg.encode(this, quality);

    var fd =  fs.openSync(name, 'w');
    var bf = Buffer.from(data);

    fs.writeSync(fd, bf, 0, bf.length);
    fs.closeSync(fd);
}

Bitmap.loadFromPNG = function (name, cc) {
    var PNG = require('./png-node.js');

    var png = PNG.load(name);
    png.decode(function (pixels) {    
        cc(new Bitmap(png.width, png.height, pixels));
    });
}

exports.Bitmap = Bitmap;
